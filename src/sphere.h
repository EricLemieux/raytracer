#pragma once

#include <math.h>
#include <glm/glm.hpp>
#include "ray.h"
#include "light.h"

class Sphere{
public:
	Sphere(double _radius, glm::vec3 _position, glm::vec3 _colour):radius(_radius), position(_position), colour(_colour){}
	~Sphere(){}

	glm::vec3 Intersect(const Ray &r, Light *lights, int numLights, float *distance);

	double radius;
	glm::vec3 position, colour;
};