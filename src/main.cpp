#include <glm/glm.hpp>

#include "ray.h"
#include "sphere.h"
#include "light.h"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>

#define WIDTH 600
#define HEIGHT 400

Sphere spheres[] = {
	Sphere(50, glm::vec3(0, 0, -50), glm::vec3(0,1,0)),
	Sphere(50, glm::vec3(-50, -50, 0), glm::vec3(0,0,1)),
	Sphere(50, glm::vec3(50, 50, 0), glm::vec3(1,0,0))
};
int numSpheres = sizeof(spheres)/sizeof(Sphere);

Light lights[] = {
	Light(glm::vec3(500,500,500))
};
int numLights = sizeof(lights)/sizeof(Light);

double Clamp(double n){
	return n>1 ? 1: n<0 ? 0: n;
}

int main(int argc, char** argv)
{
	int samples = 1;
	Ray cam(glm::vec3(0,0,250), glm::normalize(glm::vec3(0,0,-1)));
	glm::vec3 *image = new glm::vec3[WIDTH*HEIGHT]{glm::vec3(0,0,0)};

	for(int x = 0; x < WIDTH; ++x){		
		for(int y = 0; y < HEIGHT; ++y){
			float minDist = 99999999999999.9f;
			for(int i = 0; i < numSpheres; ++i){
				Ray nr = cam;
				nr.direction += glm::vec3(-int(WIDTH/2) + x,int(HEIGHT/2) - y,0);
				nr.origin += glm::vec3(-int(WIDTH/2) + x,int(HEIGHT/2) - y,0);
				float dist = 0.0f;
				glm::vec3 colour = spheres[i].Intersect(nr, lights, numLights, &dist);
				if(dist < minDist){
					image[(y*WIDTH) + x] = colour;
					minDist = dist;
				}

			}
		}
	}

	FILE *f = fopen("image.ppm", "w");
	fprintf(f, "P3\n%d %d\n%d\n",WIDTH, HEIGHT, 255);
	for(int i = 0; i < WIDTH*HEIGHT; ++i){
		fprintf(f, "%d %d %d ", int(Clamp(image[i].x)*255), int(Clamp(image[i].y)*255), int(Clamp(image[i].z)*255));
	}

	return 0;
}