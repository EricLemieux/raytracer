#include "sphere.h"

glm::vec3 Sphere::Intersect(const Ray &r, Light *lights, int numLights, float *distance){
	float delta, a,b,c;
	*distance = 9999999.9f;

	glm::vec3 o = r.origin, d = r.direction;

	a = (d.x-o.x)*(d.x-o.x) + (d.y-o.y)*(d.y-o.y) + (d.z-o.z)*(d.z-o.z);
	b = 2*((d.x-o.x)*(o.x-position.x) + (d.y-o.y)*(o.y-position.y) + (d.z-o.z)*(o.z-position.z));
	c = (o.x-position.x)*(o.x-position.x) + (o.y-position.y)*(o.y-position.y) + (o.z-position.z)*(o.z-position.z) - radius*radius;
	delta = (b*b) - (4*a*c);

	if(delta < 0.0f)
		return glm::vec3(0,0,0);
	else if(delta == 0.0f)
		return glm::vec3(0,0,0);

	//We know the ray hit so lets do something interesting with the colours we return
	float r1,r2;
	r1 = (-b + sqrt(delta))/(2*a);
	r2 = (-b - sqrt(delta))/(2*a);

	glm::vec3 p1 = glm::vec3(o.x + r1*(d.x-o.x), o.y + r1*(d.y-o.y), o.z + r1*(d.z-o.z));
	glm::vec3 p2 = glm::vec3(o.x + r2*(d.x-o.x), o.y + r2*(d.y-o.y), o.z + r2*(d.z-o.z));

	*distance = abs(glm::length(r.origin - p1));

	glm::vec3 normal = glm::normalize(position-p1);
	
	glm::vec3 viewDir = -glm::normalize(r.origin + p1);//glm::normalize(-p1);
	
	glm::vec3 specular = glm::vec3(0,0,0);
	glm::vec3 lambertian = glm::vec3(0,0,0);
	glm::vec3 ambient = glm::vec3(0.1, 0.1, 0.1);

	for(int i = 0; i < numLights; ++i){
		glm::vec3 lightPos = lights[i].position;
		lightPos.x *= -1;
		lightPos.y *= -1;
		glm::vec3 lightDir = glm::normalize(lightPos - p1);

		float lam = glm::max(glm::dot(lightDir, normal), 0.0f);

		if(lam > 0.0f){
			glm::vec3 halfDir = glm::normalize(lightDir - viewDir);
			float angle = glm::max(glm::dot(halfDir, normal), 0.0f);
			float percent = glm::pow(angle, 16);
			specular += glm::vec3(percent) * lights[i].colour;
		}
		lambertian += glm::vec3(lam);
	}

	return ambient + (lambertian*colour) + specular;
}