#pragma once

#include <glm/glm.hpp>

class Light{
public:
	Light(glm::vec3 _position=glm::vec3(0,0,0), glm::vec3 _colour=glm::vec3(1,1,1)):position(_position), colour(_colour){}

	glm::vec3 position, colour;
};