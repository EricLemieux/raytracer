#pragma once

#include <glm/glm.hpp>

class Ray{
public:
	Ray(glm::vec3 _origin, glm::vec3 _direction):origin(_origin), direction(_direction){}
	~Ray(){}

	glm::vec3 origin, direction;
};